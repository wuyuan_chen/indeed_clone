# JobFlow
A full-stack web application with the goal of consolidate all software engineering jobs in one place.
### Description
This is the frontend application where it display all the jobs, allows you to filter jobs by category and location.

### docker usage
To run the frontend application only.
```sh
docker build -t [tag-name] .
docker run --name [tag-name] -p 5173:80 -d [tag-name]
```
please refer to [here](https://gitlab.com/all-in-one-job-feed/jobflow-backend/-/blob/main/README.md?ref_type=heads) to run the fullstack application.

To enter the docker container
```sh
docker exec -it jobflow-react sh
```

### Application UI
Following image shows the result for a search for all backend engineering jobs:
![image](./app_ui/home.png)

Logged in user with bookmarks:
![image](./app_ui/logged_in.png)

Bookmark Tab:
![image](./app_ui/bookmarked.png)

