import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./userSlice";
import postReducer from "./postSlice";
import dropdownSlice from "./dropdownSlice";
import filterOptionSlice from "./filterOptionSlice";

export const store = configureStore({
    reducer: {
        user: userReducer,
        post: postReducer,
        dropdown: dropdownSlice,
        filterOption: filterOptionSlice,
    }
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;