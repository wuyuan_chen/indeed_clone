import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface DropdownState {
    [name: string]: number;
}

const initialState: DropdownState = {}
const dropdownSlice = createSlice({
    name: "selectedDropdown",
    initialState,
    reducers: {
        selectedDropdown: (state, action: PayloadAction<{name: string; selectedIndex: number}>) => {
            // dynamically add to the dictionary
            const {name, selectedIndex} = action.payload;
            state[name] = selectedIndex;
        }
    }
})

export const {selectedDropdown} = dropdownSlice.actions;
export default dropdownSlice.reducer;