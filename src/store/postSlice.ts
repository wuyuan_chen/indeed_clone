import axios from "axios";
import { 
    PayloadAction,
    createAsyncThunk,
    createSlice } from "@reduxjs/toolkit";
import { PageDetail, POSTS_API, getHeaders, SEARCH_API, BOOKMARK_API } from "../glob";


const initialState: PageDetail = {
    totalPost: 0,
    page: 1,
    perPage: 10,
    jobPosts: [],
};

const postSlice = createSlice({
    name: "post",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(postAsync.pending, () => {
                console.log("postAsync.pending")
            })
            .addCase(postAsync.fulfilled, (state, action: PayloadAction<PageDetail>) => {
                console.log("postAsync.fulfilled")
                state.totalPost = action.payload.totalPost
                state.page = action.payload.page
                state.perPage = action.payload.perPage
                state.jobPosts = [...action.payload.jobPosts]
            })
            .addCase(searchAsync.pending, () => {
                console.log("searchAsync.pending")
            })
            .addCase(searchAsync.fulfilled, (state, action: PayloadAction<PageDetail>) => {
                console.log("searchAsync.fulfilled")
                state.totalPost = action.payload.totalPost
                state.page = action.payload.page
                state.perPage = action.payload.perPage
                state.jobPosts = [...action.payload.jobPosts]
            })
            .addCase(bookmarkAsync.pending, () => {
                console.log("bookmarkAsync.pending")
            })
            .addCase(bookmarkAsync.fulfilled, (state, action: PayloadAction<PageDetail>) => {
                console.log("bookmarkAsync.fulfilled")
                state.totalPost = action.payload.totalPost
                state.page = action.payload.page
                state.perPage = action.payload.perPage
                state.jobPosts = [...action.payload.jobPosts]
            })
    }
})

export const postAsync = createAsyncThunk(
    "post/postAsync",
    async () => {
        try {
            const resp = await axios.get(POSTS_API, getHeaders());
            const pageDetail = {...resp.data};
            return pageDetail;
        } catch (err: any) {
            throw new Error("encounter error while get posts.");
        }
    }
)

export const searchAsync = createAsyncThunk(
    "search/searchAsync",
    async (queryParams: string) => {
        try {
            const resp = await axios.get(
                `${SEARCH_API}?${queryParams}`,
                getHeaders()
            );
            const pageDetail = {...resp.data};
            return pageDetail;
        } catch (err: any) {
            throw new Error("encounter error while fetch Search API")
        }
    }

)

export const bookmarkAsync = createAsyncThunk(
    "bookmark/bookmarkAsync",
    async (page: number = 1) => {
        try {
            const resp = await axios.get(
                `${BOOKMARK_API}?page=${page}`,
                getHeaders()
            );
            const pageDetail = {...resp.data};
            return pageDetail;
        } catch (err: any) {
            throw new Error("encounter error while fetch Search API")
        }
    }
)
export default postSlice.reducer;
