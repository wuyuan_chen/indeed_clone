import axios from "axios"
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit"
import { FILTER_API } from "../glob"

interface FilterOptionState {
    categories: string[],
    locations: string[]
}
const initialState: FilterOptionState = {
    categories: [],
    locations: []
}
const filterOptionSlice = createSlice({
    name: "filterOptionSlice",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(filterOptionAsync.pending, () => {
                console.log("filterOptionsAsync pending")
            })
            .addCase(filterOptionAsync.fulfilled, (state, action: PayloadAction<FilterOptionState>) => { 
                state.categories = [...action.payload.categories]
                state.locations = [...action.payload.locations] 
            })

    },
})


export const filterOptionAsync = createAsyncThunk(
    "filter/filterOptionAsync",
    async () => {
        try {
            const resp = await axios.get(FILTER_API);
            console.log(resp.data)
            return {...resp.data}
        } catch (err: any) {
            throw new Error("encounter error while getting filter options")
        }
    }
)

export default filterOptionSlice.reducer;