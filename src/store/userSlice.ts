import axios from "axios";
import { 
    PayloadAction,
    createAsyncThunk,
    createSlice } from "@reduxjs/toolkit";

import { UserState, UserLoginData, getHeaders, LOGIN_API, CHECK_AUTH_API, LOGOUT_API } from "../glob";

const headers = getHeaders();

const initialState: UserState = {
    isAuthenticated: false,
    userData: {
        username: "",
        email: "",
        firstName: "",
        lastName: "",
    },
    errorLog: "",
}

const loginSlice = createSlice({
    name: "user",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(loginAsync.pending, () => {
                        console.log("loginAsync.pending")
                    })
            .addCase(loginAsync.fulfilled, (state, action:PayloadAction<UserState>) => {
                console.log("loginAsync.fulfilled.")
                state.isAuthenticated = action.payload.isAuthenticated;
                state.userData = action.payload.userData;
                })
            .addCase(loginAsync.rejected, (state, action: any ) => {
                console.log("loginAsync.rejected.", action.payload.detail);
                state.isAuthenticated = false;
                state.userData = initialState.userData;
                state.errorLog = action.payload.detail;
                console.log(state.errorLog);
                })
            .addCase(logoutAsync.pending, () => {
                console.log("logoutAsync.pending")
            })
            .addCase(logoutAsync.fulfilled, (state, action: PayloadAction<UserState>) => {
                console.log("logoutAsync.fulfilled")
                state.isAuthenticated = action.payload.isAuthenticated;
                state.userData = action.payload.userData;
            })
            .addCase(checkAuthAsync.pending, () => {
                console.log("checkAuthAsync.pending")
            })
            .addCase(checkAuthAsync.fulfilled, (state, action: PayloadAction<UserState>) => {
                console.log("checkAuthAsync.fulfilled.")
                state.isAuthenticated = action.payload.isAuthenticated;
                state.userData = action.payload.userData;
            })
    }
})

export const loginAsync = createAsyncThunk(
    "user/loginAsync",
    async ({username, password}: UserLoginData, { rejectWithValue }) => {
      // Make a POST request to your backend

      try {

        const response = await axios.post(
            LOGIN_API,
            {username: username, password: password},
            {withCredentials: true}
        );

        const respUserData = response.data.user;
        
        const userData: UserState = {
            isAuthenticated: true,
            userData: {
                username: respUserData.username,
                email: respUserData.email,
                firstName: respUserData.first_name,
                lastName: respUserData.last_name,

            },
            errorLog: "",
        }
        return userData;

      } catch (err: any) {
        if (!err.response) {
            throw err;
        }
        return rejectWithValue(err.response.data)
      }
    }
)

export const logoutAsync = createAsyncThunk(
    "user/logoutAsync",
    async () => {
        try {
            await axios.get(
                LOGOUT_API,
                headers,
            );
        } catch (err: any) {
             throw new Error("Encounter error while logging out.");
        }
        const userData: UserState = {
            isAuthenticated: false,
            userData: {
                username: "",
                email: "",
                firstName: "",
                lastName: "",

            },
            errorLog: "",
        }
        return userData;
    }
)

export const checkAuthAsync = createAsyncThunk(
    'user/checkAuth',
    async () => {
        const initialState: UserState = {
            isAuthenticated: false,
            userData: {
                username: "",
                email: "",
                firstName: "",
                lastName: "",
            },
            errorLog: "",
        }
        const response = await axios.get(
            CHECK_AUTH_API,
            headers,
        );
        const respUserData = response.data.user;
        if (respUserData.username !== "") {
            initialState.isAuthenticated = true;
            initialState.userData = {
                username: respUserData.username,
                email: respUserData.email,
                firstName: respUserData.first_name,
                lastName: respUserData.last_name,
            }

        }

        return initialState;
})

export default loginSlice.reducer;