import { useState } from "react";
import CardList from "./CardList";
import { JobDetail } from "../glob";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../store/store";
import { bookmarkAsync, postAsync } from "../store/postSlice";

interface Props {
  jobPosts: JobDetail[];
  setTabIndex: (index: number) => void;
}
export default function TabSection({ jobPosts, setTabIndex }: Props) {
  const sections = ["Latest Job Feed", "Your Bookmark"];
  const [selectedIndex, setSelectedIndex] = useState(0);
  const postDispatch = useDispatch<AppDispatch>();
  const user = useSelector((state: RootState) => state.user);

  return (
    <>
      <div className="col col-lg-auto">
        <ul
          className="nav nav-tabs justify-content-center pb-3"
          id="myTab"
          role="tablist"
        >
          {sections.map((item, index) => (
            <li
              className="nav-item"
              style={
                selectedIndex == index ? { borderBottom: "1px solid" } : {}
              }
              role="presentation"
              key={index}
              onClick={() => {
                setSelectedIndex(index);
                setTabIndex(index);
                if (index == 0) {
                  postDispatch(postAsync());
                } else {
                  postDispatch(bookmarkAsync(1));
                }
              }}
            >
              <button
                className={
                  selectedIndex == index
                    ? "nav-link active fw-bold"
                    : "nav-link"
                }
                id={index.toString()}
                data-bs-toggle="tab"
                data-bs-target={"#tab" + index.toString()}
                type="button"
                role="tab"
                aria-controls={index.toString()}
                aria-selected={selectedIndex == index ? true : false}
              >
                {item}
              </button>
            </li>
          ))}
        </ul>
      </div>

      <div className="tab-content" id="myTabContent">
        {selectedIndex == 1 && !user.isAuthenticated ? (
          "Please login to view your bookmarks!"
        ) : (
          <CardList jobPosts={jobPosts} />
        )}
      </div>
    </>
  );
}
