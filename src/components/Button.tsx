interface Props {
  color?: "primary" | "secondary" | "danger";
  name: string;
}
function Button({ color = "primary", name }: Props) {
  return (
    <button type="button" className={"btn btn-outline-" + color}>
      {name}
    </button>
  );
}

export default Button;
