import { CSSProperties, useState } from "react";
import { useDispatch } from "react-redux";
import { AppDispatch } from "../store/store";
import { selectedDropdown } from "../store/dropdownSlice";
interface Props {
  color?: "primary" | "secondary" | "danger" | "dark";
  title: string;
  options: string[];
}

export default function Dropdown({ color = "primary", title, options }: Props) {
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const dropdownDispatch = useDispatch<AppDispatch>();
  // dynamically set the scroll option
  const scroll_class = (): CSSProperties => {
    if (options.length >= 9) {
      return { maxHeight: "300px", overflowY: "auto" };
    }
    return {};
  };
  return (
    <>
      <div className="dropdown">
        <button
          className={"btn btn-outline-" + color + " dropdown-toggle w-100"}
          type="button"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          {selectedIndex == -1 ? title : options[selectedIndex]}
        </button>
        <ul className="dropdown-menu" style={scroll_class()}>
          {options.map((item, index) => (
            <li
              key={index}
              onClick={() => {
                setSelectedIndex(index);
                dropdownDispatch(
                  selectedDropdown({
                    name: title,
                    selectedIndex: index,
                  })
                );
              }}
            >
              <a className="dropdown-item" href="#">
                {item}
              </a>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}
