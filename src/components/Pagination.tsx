import { useState } from "react";

interface Props {
  totalPages: number;
  onPageChange: (page: number) => void;
}
export default function Pagination(props: Props) {
  const { totalPages, onPageChange } = props;
  const [selectedIndex, setSelectedIndex] = useState(1);

  const pageItems = [];
  const onClick = (page: number) => {
    onPageChange(page);
  };
  for (let i = 1; i <= totalPages; i++) {
    pageItems.push(
      <li
        key={i}
        className={selectedIndex == i ? "page-item active" : "page-item"}
        aria-current={selectedIndex == i && "page"}
      >
        <a
          className="page-link"
          href="#"
          onClick={() => {
            setSelectedIndex(i);
            onClick(i);
          }}
        >
          {i}
        </a>
      </li>
    );
  }
  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination flex-wrap">
        <li className="page-item">
          <a
            className="page-link"
            href="#"
            onClick={() => {
              if (selectedIndex <= 1) return;
              const newIndex = selectedIndex - 1;
              setSelectedIndex(newIndex);
              onClick(newIndex);
            }}
          >
            Previous
          </a>
        </li>
        {pageItems}
        <li className="page-item">
          <a
            className="page-link"
            href="#"
            onClick={() => {
              if (selectedIndex >= totalPages) return;
              const newIndex = selectedIndex + 1;
              setSelectedIndex(newIndex);
              onClick(newIndex);
            }}
          >
            Next
          </a>
        </li>
      </ul>
    </nav>
  );
}
