import Card from "./Card";
import { JobDetail } from "../glob";

interface Props {
  jobPosts: JobDetail[];
}

function get_jobInfo(item: JobDetail) {
  const unique_key = Math.random().toString(36).substring(2);
  return (
    <div className="col-lg-4 col-md-6" key={unique_key} id={item.id.toString()}>
      <Card jobDetail={item} />
    </div>
  );
}

export default function CardsList({ jobPosts }: Props) {
  return (
    <>
      <div className="row">{jobPosts.map((item) => get_jobInfo(item))}</div>
    </>
  );
}
