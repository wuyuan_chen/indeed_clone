import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../store/store";
import { Link } from "react-router-dom";
import { logoutAsync } from "../store/userSlice";

function NavBar() {
  const user = useSelector((state: RootState) => state.user);
  const userDispatch = useDispatch<AppDispatch>();

  const handleLogout = async () => {
    userDispatch(logoutAsync());
  };
  return (
    <>
      <nav className="navbar navbar-expand-lg bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">
            JobFlow
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link active" aria-current="page" to="/">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                {/* <Link className="nav-link active" to="/profile">
                  Profile
                </Link> */}
              </li>
            </ul>
            <div className="d-flex" role="search">
              {user.isAuthenticated ? (
                <div className="d-flex">
                  <a className="nav-link">hello {user.userData.firstName},</a>
                  <a className="nav-link" onClick={handleLogout} href="#">
                    logout
                  </a>
                </div>
              ) : (
                <Link className="nav-link" to="/login">
                  Login
                </Link>
              )}
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}

export default NavBar;
