import axios from "axios";
import { useEffect, useState } from "react";
import { JobDetail, getHeaders, BOOKMARK_API } from "../glob";

interface CardProps {
  jobDetail: JobDetail;
}

function get_detail(item: string, index: number) {
  return (
    <div className="col pe-0" key={index}>
      <p className="card-text">{item}</p>
    </div>
  );
}

function get_locations(location: string, index: number) {
  return (
    <p className="col-auto card-text mb-0 pe-0" key={index}>
      {location}
    </p>
  );
}

function Card({ jobDetail }: CardProps) {
  const {
    id,
    title,
    company,
    locations,
    detail,
    job_link,
    source,
    is_bookmarked,
  } = jobDetail;

  const [isBookmarked, setIsBookmarked] = useState<boolean>(is_bookmarked);
  useEffect(() => {}, [isBookmarked]);
  const handleClick = async (e: React.FormEvent) => {
    e.preventDefault();
    const post_id = id.toString();
    const headers = getHeaders();
    if (isBookmarked) {
      try {
        await axios.delete(`${BOOKMARK_API}${post_id}/`, headers);
      } catch (err: any) {
        throw new Error("unable to delete bookmark.");
      }
    } else {
      try {
        await axios.post(BOOKMARK_API, { posts: [post_id] }, headers);
      } catch (err: any) {
        throw new Error("unable to save bookmark.");
      }
    }
    // Toggle bookmark state locally without waiting for the API response
    setIsBookmarked((prevIsBookmarked) => !prevIsBookmarked);
  };

  return (
    <>
      <div className="row pb-3">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <div className="d-flex justify-content-between align-items-center">
                <h5 className="card-title mb-0">
                  <a className="link_option" href={job_link}>
                    {title}
                  </a>
                </h5>
                <a href="#" id={id.toString()} onClick={handleClick}>
                  <img
                    className="bookmark-icon"
                    src={
                      isBookmarked
                        ? "/assets/icons/bookmark_solid.svg"
                        : "/assets/icons/bookmark_reg.svg"
                    }
                    alt="My SVG Icon"
                  />
                </a>
              </div>
              <p className="card-text mb-0">{company}</p>
              <div className="row">{locations.map(get_locations)}</div>
              <div className="row row-cols-auto">{detail.map(get_detail)}</div>
              <p className="card-text">from: {source}</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Card;
