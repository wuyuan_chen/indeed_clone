import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../store/store";
import { searchAsync } from "../store/postSlice";
import Dropdown from "./Dropdown";
import { filterOptionAsync } from "../store/filterOptionSlice";
import { getQueryParams } from "../glob";

export default function SearchSection() {
  const page = 1;
  const jobCategories = useSelector(
    (state: RootState) => state.filterOption.categories
  );
  const jobLocations = useSelector(
    (state: RootState) => state.filterOption.locations
  );
  const dropdownIndex = useSelector((state: RootState) => state.dropdown);
  const postDispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    postDispatch(filterOptionAsync());
  }, []);

  const getJobPosts = async () => {
    postDispatch(
      searchAsync(
        getQueryParams(jobCategories, jobLocations, dropdownIndex, page)
      )
    );
  };
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-5 text-end pb-3">
            <Dropdown color="dark" title="Category" options={jobCategories} />
          </div>
          <div className="col-md-5 text-start pb-3">
            <Dropdown color="dark" title="Location" options={jobLocations} />
          </div>
          <div className="col-md-2 text-start">
            <button
              type="button"
              className="btn btn-outline-primary"
              onClick={getJobPosts}
            >
              Search
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
