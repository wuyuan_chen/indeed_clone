import { Route, Routes } from "react-router-dom";
import NavBar from "./components/NavBar";
import Home from "./pages/home";
import Login from "./pages/login";
import Register from "./pages/register";
import { useDispatch } from "react-redux";
import { AppDispatch } from "./store/store";
import { checkAuthAsync } from "./store/userSlice";

function App() {
  // initialize the state upon start of application
  const userDispatch = useDispatch<AppDispatch>();
  userDispatch(checkAuthAsync());
  return (
    <>
      <NavBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
      </Routes>
    </>
  );
}

export default App;
