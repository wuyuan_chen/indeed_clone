import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../store/store";
import { loginAsync } from "../store/userSlice";

export default function Login() {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });

  const user = useSelector((state: RootState) => state.user);
  const userDispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    // This effect will run whenever user.isAuthenticated changes
    if (user.isAuthenticated) {
      console.log("login successful");
      navigate("/");
    }
  }, [user.isAuthenticated]);

  // Handle input changes
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.id]: e.target.value,
    });
  };

  // Handle form submission
  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      // use a dispatch call to backend to login
      userDispatch(loginAsync(formData));
    } catch (error) {
      // Handle errors (show an error message, log, etc.)
      console.error("login failed", error);
    }
  };

  return (
    <>
      <div className="container p-5">
        {user.errorLog && (
          <div className="alert alert-danger" role="alert">
            {user.errorLog}
          </div>
        )}
        <main className="form-signin">
          <form onSubmit={handleSubmit}>
            {/* <img
              className="mb-4"
              src="/docs/5.0/assets/brand/bootstrap-logo.svg"
              alt=""
              width="72"
              height="57"
            /> */}
            <h1 className="h3 mb-3 fw-normal">Please sign in</h1>

            <div className="form-floating">
              <input
                type="text"
                className="form-control"
                id="username"
                value={formData.username}
                placeholder="Username"
                onChange={handleInputChange}
              />
              <label htmlFor="username">User Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="password"
                className="form-control"
                id="password"
                value={formData.password}
                placeholder="Password"
                onChange={handleInputChange}
              />
              <label htmlFor="password">Password</label>
            </div>

            <button className="w-100 btn btn-lg btn-primary" type="submit">
              Sign in
            </button>
            <div className="text-center pt-3">
              <p>
                Not a member?{" "}
                <Link style={{ textDecoration: "none" }} to="/register">
                  Register
                </Link>
              </p>
            </div>
          </form>
        </main>
      </div>
    </>
  );
}
