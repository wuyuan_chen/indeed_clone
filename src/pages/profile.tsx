import axios from "axios";
import { getHeaders, PROFILE_API } from "../glob";

export default function Profile() {
  const headers = getHeaders();

  const handleLogout = async () => {
    try {
      const response = await axios.get(PROFILE_API, headers);
      console.log(response);
    } catch (e: any) {
      console.log("error: ", e.response);
    }
  };
  try {
    handleLogout();
  } catch (e: any) {
    console.log(e.response);
  }
  return <h1> this is profile!!! </h1>;
}
