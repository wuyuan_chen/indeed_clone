import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { REGISTER_API } from "../glob";

export default function Register() {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
    first_name: "",
    last_name: "",
  });
  // Handle input changes
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.id]: e.target.value,
    });
  };

  // Handle form submission
  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      // Make a POST request to your backend
      const response = await axios.post(REGISTER_API, formData);

      // Handle the response (you can redirect or show a success message)
      console.log("Registration successful", response.data);
      navigate("/login");
    } catch (error) {
      // Handle errors (show an error message, log, etc.)
      console.error("Registration failed", error);
    }
  };
  return (
    <>
      <div className="container p-5">
        <main className="form-signin">
          <form onSubmit={handleSubmit}>
            {/* <img
              className="mb-4"
              src="/docs/5.0/assets/brand/bootstrap-logo.svg"
              alt=""
              width="72"
              height="57"
            /> */}
            <h1 className="h3 mb-3 fw-normal">Please Register</h1>

            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="username"
                value={formData.username}
                placeholder="User Name"
                onChange={handleInputChange}
              />
              <label htmlFor="username">User Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="email"
                className="form-control"
                id="email"
                value={formData.email}
                placeholder="name@example.com"
                onChange={handleInputChange}
              />
              <label htmlFor="email">Email address</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="password"
                className="form-control"
                id="password"
                value={formData.password}
                placeholder="Password"
                onChange={handleInputChange}
              />
              <label htmlFor="password">Password</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="first_name"
                value={formData.first_name}
                placeholder="First Name"
                onChange={handleInputChange}
              />
              <label htmlFor="firstName">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="last_name"
                value={formData.last_name}
                placeholder="Last Name"
                onChange={handleInputChange}
              />
              <label htmlFor="lastName">Last Name</label>
            </div>

            <button className="w-100 btn btn-lg btn-primary" type="submit">
              Register
            </button>
            <div className="text-center pt-3">
              <p>
                Already a member?{" "}
                <Link style={{ textDecoration: "none" }} to="/login">
                  Login
                </Link>
              </p>
            </div>
          </form>
        </main>
      </div>
    </>
  );
}
