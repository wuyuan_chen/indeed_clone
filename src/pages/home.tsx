import { useEffect, useState } from "react";
import SearchSection from "../components/SearchSection";
import Pagination from "../components/Pagination";
import TabSection from "../components/TabSection";
import { PageDetail } from "../glob";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../store/store";
import { bookmarkAsync, postAsync } from "../store/postSlice";
import { searchAsync } from "../store/postSlice";
import { getQueryParams } from "../glob";

export default function Home() {
  const pageDetail = useSelector((state: RootState) => state.post);
  const user = useSelector((state: RootState) => state.user);
  const postDispatch = useDispatch<AppDispatch>();
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const jobCategories = useSelector(
    (state: RootState) => state.filterOption.categories
  );
  const jobLocations = useSelector(
    (state: RootState) => state.filterOption.locations
  );
  const dropdownIndex = useSelector((state: RootState) => state.dropdown);

  const defaultPageDetail: PageDetail = {
    totalPost: 0,
    page: 1,
    perPage: 10,
    jobPosts: [],
  };
  useEffect(() => {
    postDispatch(postAsync());
  }, []);

  const onPageChange = (page: number) => {
    selectedTabIndex == 0
      ? postDispatch(
          searchAsync(
            getQueryParams(jobCategories, jobLocations, dropdownIndex, page)
          )
        )
      : postDispatch(bookmarkAsync(page));
  };

  const totalPages = Math.ceil(
    pageDetail.totalPost / defaultPageDetail.perPage
  );
  return (
    <>
      <div className="container p-5">
        <div className="row pb-5">
          <div className="col">
            <SearchSection />
          </div>
        </div>
        <div className="row justify-content-center">
          <TabSection
            jobPosts={pageDetail.jobPosts}
            setTabIndex={setSelectedTabIndex}
          />
          {selectedTabIndex == 1 && !user.isAuthenticated ? (
            <></>
          ) : (
            <Pagination totalPages={totalPages} onPageChange={onPageChange} />
          )}
        </div>
      </div>
    </>
  );
}
