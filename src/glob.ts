const APP_URL = `${import.meta.env.VITE_REACT_APP_API_URL}/`
const API_URL = `${APP_URL}api/`
export const BOOKMARK_API = `${API_URL}bookmark/`
export const FILTER_API = `${API_URL}filter/`
export const SEARCH_API = `${API_URL}search/`
export const POSTS_API = `${API_URL}posts/`
export const PROFILE_API = `${API_URL}profile/`
export const LOGIN_API = `${API_URL}login/`
export const REGISTER_API = `${API_URL}register/`
export const LOGOUT_API = `${API_URL}logout/`
export const CHECK_AUTH_API = `${API_URL}check-auth/`

export interface PageDetail {
  totalPost: number;
  page: number;
  perPage: number;
  jobPosts: JobDetail[];
}
export interface JobDetail {
  id: number;
  title: string;
  company: string;
  locations: string[];
  detail: string[];
  job_link: string;
  source: string;
  is_bookmarked: boolean;
}

export interface UserData {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
}

export interface UserLoginData {
    username: string;
    password: string;
}

export interface UserState {
    isAuthenticated: Boolean;
    userData: UserData;
    errorLog: string;
}


export function getCookie(name: string) {
    const value = `; ${document.cookie}`;
    const match = value.match(new RegExp(`${name}=([^;]+)`));
    return match ? match[1] : null;
}

export function getHeaders() {
  const headers = {
    headers: {
      "X-CSRF-TOKEN": getCookie("csrftoken"),
    },
    withCredentials: true,
  };

  return headers;
}

interface DropdownState {
    [name: string]: number;
}

export function getQueryParams(jobCategories: string[], jobLocations: string[], dropdownIndex: DropdownState, page: number) {
  const queryParams = new URLSearchParams();
  queryParams.set("page", page.toString());
  const categoryIndex =
    dropdownIndex["Category"] === undefined ? -1 : dropdownIndex["Category"];
  const locationIndex =
    dropdownIndex["Location"] === undefined ? -1 : dropdownIndex["Location"];
  if (categoryIndex == -1 && locationIndex == -1) return queryParams.toString();

  const selectedCategory = jobCategories[categoryIndex];
  const selectedLocation = jobLocations[locationIndex];
    if (selectedCategory) queryParams.set("category", selectedCategory);
    if (selectedLocation) queryParams.set("location", selectedLocation);

    return queryParams.toString()
}
